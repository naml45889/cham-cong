package com.example.demo.interceptor;

import com.example.demo.model.User;
import com.example.demo.service.UserService;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class UserInterceptor extends HandlerInterceptorAdapter {
    @Autowired
    private UserService userService;

    @Override
    // cho user
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
        String token = request.getHeader("token");
        byte[] decodedBytes = Base64.decode(token);
        if (decodedBytes == null) {
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
            return false;
        }
        String decodedString = new String(decodedBytes);
        String[] userDTo = decodedString.split(" ");
        String username = userDTo[0];
        String password = userDTo[1];
        User user = userService.userByUsername(username);
        if (password.equals(user.getPassword())) {
            return true;
        }
        System.out.println("Pre Handle for post method is calling.");
        response.setStatus(HttpServletResponse.SC_FORBIDDEN);
        return false;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
                           ModelAndView modelAndView) throws Exception {
        System.out.println("post handle");
        super.postHandle(request, response, handler, modelAndView);
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
            throws Exception {
        System.out.println("after completion");
        super.afterCompletion(request, response, handler, ex);
    }

}
