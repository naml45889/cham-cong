package com.example.demo.repository;

import com.example.demo.model.Managerment;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ManagermentRepository extends CrudRepository<Managerment, Integer> {
    List<Managerment> findByUserId(int userId);
}
