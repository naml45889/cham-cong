package com.example.demo.repository;

import com.example.demo.model.Access;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccessRepository extends CrudRepository<Access, Integer> {
    List<Access> findByUserId(int userId);
}
