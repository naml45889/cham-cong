package com.example.demo.repository;

import com.example.demo.model.Request;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RequestRepository extends CrudRepository<Request, Integer> {
    List<Request> findByUserId(int userId);
}
