package com.example.demo.repository;

import com.example.demo.model.UserPosition;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserPositionRepository extends CrudRepository<UserPosition, Integer> {
}
