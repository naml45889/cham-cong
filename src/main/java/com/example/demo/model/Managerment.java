package com.example.demo.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "managerment")
@Data
@NoArgsConstructor
public class Managerment {
    @Id
    @GeneratedValue
    private int id;
    private Date created_at;
    private String created_by;
    private Date updated_at;
    private String updated_by;
    @ManyToOne
    @JoinColumn(name = "manager_id")
    private User manager;
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
}
