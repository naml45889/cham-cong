package com.example.demo.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "userposition")
@Data
@NoArgsConstructor
public class UserPosition {
    @Id
    @GeneratedValue
    private int id;
    private String name;
    private int level;
    private Date created_at;
    private String created_by;
    private Date updated_at;
    private String updated_by;
}
