package com.example.demo.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Date;

@Entity
@Table(name = "access")
@Data
@NoArgsConstructor
public class Access {
    @Id
    @GeneratedValue
    private int id;
    private LocalDate date;
    private LocalTime time;
    private Date created_at;
    private String created_by;
    private Date updated_at;
    private String updated_by;
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
}
