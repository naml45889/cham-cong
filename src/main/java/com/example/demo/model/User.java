package com.example.demo.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "user")
@NoArgsConstructor
@Data
public class User {
    @Id
    @GeneratedValue
    private int id;
    private String name;
    private boolean sex;
    @OneToOne
    @JoinColumn(name = "departmentId")
    private Department department;
    @OneToOne
    @JoinColumn(name = "positionId")
    private UserPosition position;
    private int dayOff;
    private int oT;
    private LocalTime checkIn;
    private LocalTime checkOut;
    private LocalTime timeBreak;
    private boolean isAdmin;
    @Size(min = 5,max = 30)
    private String password;
    @Size(min = 5,max = 30)
    private String username;
    @Size(min = 5,max = 30)
    private String email;
    private LocalDate birthDay;
    private LocalDate checkInDate;
    private Date created_at;
    private String created_by;
    private Date updated_at;
    private String updated_by;
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<Request> requests;
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<Access> accesses;
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<Managerment> managerments;

}
