package com.example.demo.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.Date;

@Entity
@Table(name = "request")
@Data
@NoArgsConstructor
public class Request {
    @Id
    @GeneratedValue
    private int id;
    private LocalDate start;
    private LocalDate end;
    @Size(min = 5,max = 30)
    private String reason;
    @Column(name = "status")
    public Status status = Status.NEW;

    public enum Status {
        NEW, APPROVED, REJECT
    }

    private Date created_at;
    private String created_by;
    private Date updated_at;
    private String updated_by;
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
}
