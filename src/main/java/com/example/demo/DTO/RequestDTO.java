package com.example.demo.DTO;

import com.example.demo.model.Request;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RequestDTO {
    public Request.Status status = Request.Status.NEW;
}
