package com.example.demo.DTO;

import com.example.demo.model.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class StatisticalDTO {
    private int id;
    private String name;
    private int position;
    private String email;
    private LocalDate birthDay;
    private float totalHour;
    private float lateHour;
    private int ot;
    private int totalDayOff;
    public void setUser(User user){
        this.id=user.getId();
        this.name=user.getName();
        this.position=user.getPosition().getId();
        this.email=user.getEmail();
        this.birthDay=user.getBirthDay();
    }
}
