package com.example.demo.DTO;

import com.example.demo.model.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserdDTO {
    private int id;
    private String name;
    private String email;
    private int position;
    private boolean sex;
    private LocalDate birthDay;

    public void setUser(User user){
        this.id=user.getId();
        this.name=user.getName();
        this.email=user.getEmail();
        this.position=user.getPosition().getId();
        this.sex=user.isSex();
        this.birthDay=user.getBirthDay();
    }
}
