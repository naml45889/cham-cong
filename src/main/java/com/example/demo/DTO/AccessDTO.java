package com.example.demo.DTO;

import com.example.demo.model.Access;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccessDTO {
    private LocalDate date;
    private LocalTime time;
    private String name;
    private String email;
    private int position;
    private String username;

    public void setAccess(Access access){
        this.date=access.getDate();
        this.time=access.getTime();
        this.name=access.getUser().getName();
        this.email=access.getUser().getEmail();
        this.position=access.getUser().getPosition().getId();
        this.username=access.getUser().getUsername();
    }
}
