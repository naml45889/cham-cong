package com.example.demo.DTO;

import com.example.demo.model.Request;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RequestdDTO {
    public Request.Status status;
    private LocalDate start;
    private LocalDate end;
    private String reason;
    private String name;
    private String email;
    private int position;

    public void setRequest(Request request){
        this.status=request.getStatus();
        this.start=request.getStart();
        this.end=request.getEnd();
        this.reason=request.getReason();
        this.name=request.getUser().getName();
        this.email=request.getUser().getEmail();
        this.position=request.getUser().getPosition().getId();
    }
}
