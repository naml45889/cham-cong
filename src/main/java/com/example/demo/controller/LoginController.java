package com.example.demo.controller;

import com.example.demo.DTO.UserDTO;
import com.example.demo.model.User;
import com.example.demo.service.UserService;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping(path = "/login", produces = "application/json")
public class LoginController {
    @Autowired
    private UserService userService;

    @PostMapping
    public ResponseEntity<String> loginUser(@RequestBody UserDTO user, HttpServletResponse response) {
        String username = user.getUsername();
        String password = user.getPassword();
        String token;
        User user1 = userService.userByUsername(username);
        if (user1 != null) {
            String token1 = username + " " + password;
            token = Base64.encode(token1.getBytes());
            response.addHeader("token", token);
            return ResponseEntity.ok("dang nhap thanh cong");
        }
        return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }
}
