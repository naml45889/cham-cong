package com.example.demo.controller;

import com.example.demo.DTO.RequestDTO;
import com.example.demo.DTO.RequestdDTO;
import com.example.demo.model.Request;
import com.example.demo.service.RequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(path = "/request", produces = "application/json")
public class RequestController {
    @Autowired
    private RequestService requestService;

    @PostMapping
    public ResponseEntity<RequestdDTO> createRequest(@RequestParam("id") int id, @Valid @RequestBody Request request, @RequestHeader("token") String token) {
        RequestdDTO r=requestService.createRequest(id,token,request);
        if (r == null) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(r);
    }

    @PutMapping
    public ResponseEntity<RequestdDTO> updateRequest(@RequestBody RequestDTO requestDTO, @RequestParam("id") int id,@RequestHeader("token") String token) {
        RequestdDTO r = requestService.changeRequest(id,token,requestDTO);
        if (r == null) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(r);
    }
}
