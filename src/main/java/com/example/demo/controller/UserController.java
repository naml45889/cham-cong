package com.example.demo.controller;

import com.example.demo.DTO.UserdDTO;
import com.example.demo.model.User;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import javax.validation.Valid;

@RestController
@RequestMapping("")
public class UserController {
    @Autowired
    private UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/user/get/")
    public ResponseEntity<Iterable<UserdDTO>> getAllusers(@RequestParam("page") int page) {
        return ResponseEntity.ok(userService.getAlluser(page));
    }


    @GetMapping("/user/")
    public ResponseEntity<UserdDTO> userbyId(@RequestParam("id") int id) {
        User user = userService.userById(id);
        UserdDTO userdDTO=new UserdDTO();
        userdDTO.setUser(user);
        return ResponseEntity.ok(userdDTO);
    }

    @PutMapping("/user/")
    public ResponseEntity<User> changeUser(@RequestParam("id") int id,@Valid @RequestBody User user) {
        user.setId(id);
        return ResponseEntity.ok((userService.changeUser(user)));
    }

    @PostMapping("/user/")
    public ResponseEntity<User> createUser(@Valid @RequestBody User user, BindingResult result) {
        if(result.hasErrors()){
            return new ResponseEntity<>(null,HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(userService.createUser(user));
    }

    @Transactional
    @DeleteMapping("/user/")
    public void deleteUser(@RequestParam("id") int id) {
        try {
            userService.deleteUser(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
