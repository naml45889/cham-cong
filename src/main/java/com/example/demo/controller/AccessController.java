package com.example.demo.controller;

import com.example.demo.DTO.AccessDTO;
import com.example.demo.service.AccessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/check", produces = "application/json")
public class AccessController {
    @Autowired
    private AccessService accessService;

    @PostMapping("/")
    public ResponseEntity<AccessDTO> createAccess(@RequestHeader("token") String token) {
        AccessDTO accessDTO=accessService.createAccess(token);
        if(accessDTO==null){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(accessDTO);
    }

}
