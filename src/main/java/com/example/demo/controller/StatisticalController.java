package com.example.demo.controller;

import com.example.demo.DTO.StatisticalDTO;
import com.example.demo.service.StatiscalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;

@RestController
@RequestMapping(path = "/statistical", produces = "application/json")
public class StatisticalController {
    @Autowired
    private StatiscalService statiscalService;

    @GetMapping("/")
    public ResponseEntity<StatisticalDTO> getStatisticalOfUser(@RequestParam("id") int id) throws ParseException {
        StatisticalDTO statisticalDTO = statiscalService.findStatiscalByUser(id);
        return ResponseEntity.ok(statisticalDTO);
    }
}
