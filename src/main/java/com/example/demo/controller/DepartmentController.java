package com.example.demo.controller;

import com.example.demo.DTO.DepartmentDTO;
import com.example.demo.service.DepartmentService;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/department", produces = "application/json")
public class DepartmentController {
    @Autowired
    private DepartmentService departmentService;
    @Autowired
    private UserService userService;

    @GetMapping("/")
    public ResponseEntity<DepartmentDTO> getOneDepartmentDTO(@RequestParam("id") int id) {
        DepartmentDTO departmentDTO= departmentService.departmentById(id);
        return ResponseEntity.ok(departmentDTO);
    }
}
