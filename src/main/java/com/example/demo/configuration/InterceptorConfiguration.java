package com.example.demo.configuration;

import com.example.demo.interceptor.AdminInterceptor;
import com.example.demo.interceptor.UserInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Component
public class InterceptorConfiguration implements WebMvcConfigurer {
    @Autowired
    UserInterceptor userInterceptor;
    @Autowired
    AdminInterceptor adminInterceptor;
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // TODO Auto-generated method stub
        registry.addInterceptor(userInterceptor).addPathPatterns("/*").excludePathPatterns("/login").excludePathPatterns("/department/");
        registry.addInterceptor(adminInterceptor).addPathPatterns("/user/").addPathPatterns("/user/get/").addPathPatterns("/statistical/").addPathPatterns("/department/");
    }
}
