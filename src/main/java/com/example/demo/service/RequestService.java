package com.example.demo.service;

import com.example.demo.DTO.RequestDTO;
import com.example.demo.DTO.RequestdDTO;
import com.example.demo.model.Managerment;
import com.example.demo.model.Request;
import com.example.demo.model.User;
import com.example.demo.repository.ManagermentRepository;
import com.example.demo.repository.RequestRepository;
import com.example.demo.repository.UserRepository;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RequestService {
    @Autowired
    private RequestRepository requestRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ManagermentRepository managermentRepository;

    public Iterable<Request> getAllRequest() {
        return requestRepository.findAll();
    }

    public Request requestById(int id) {
        Optional<Request> Request = requestRepository.findById(id);
        if (Request.isPresent()) {
            return Request.get();
        }
        return null;
    }
    public RequestdDTO createRequest(int id,String token,Request request) {
        byte[] decodedBytes = Base64.decode(token);
        if (decodedBytes == null) {
            return null;
        }
        String decodedString = new String(decodedBytes);
        String[] userDTo = decodedString.split(" ");
        String username = userDTo[0];
        String password = userDTo[1];
        Optional<User> u = userRepository.findById(id);
        request.setUser(u.get());
        Request.Status status= Request.Status.NEW;
        request.setStatus(status);
        RequestdDTO requestdDTO=new RequestdDTO();
        requestdDTO.setRequest(request);
        if (username.equals(u.get().getUsername()) && password.equals(u.get().getPassword())) {
            requestRepository.save(request);
            return requestdDTO;
        }
        return null;
    }

    public RequestdDTO changeRequest(int id,String token,RequestDTO requestDTO) {
        byte[] decodedBytes = Base64.decode(token);
        if (decodedBytes == null) {
            return null;
        }
        RequestdDTO requestdDTO=new RequestdDTO();
        Optional<Request> request;
        String decodedString = new String(decodedBytes);
        String[] userDTo = decodedString.split(" ");
        String username = userDTo[0];
        String password = userDTo[1];
        Optional<User> user = userRepository.findByUsername(username);// quan ly
        Optional<Request> request2 = requestRepository.findById(id);
        int userId = request2.get().getUser().getId();// nhan vien
        List<Managerment> managerments = (List<Managerment>) managermentRepository.findByUserId(userId);
        for (int i = 0; i < managerments.size(); i++) {
            if (password.equals(user.get().getPassword()) && userId == managerments.get(i).getUser().getId() && user.get().getId() == managerments.get(i).getManager().getId()) {
                Request.Status status = requestDTO.getStatus();
                request = requestRepository.findById(id);
                request.get().setStatus(status);
                requestdDTO.setRequest(request.get());
                requestRepository.save(request.get());
                return requestdDTO;
            }
        }
        return requestdDTO;
    }

    public void deleteRequest(int id) {
        try {
            requestRepository.deleteById(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
