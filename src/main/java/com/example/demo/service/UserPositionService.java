package com.example.demo.service;

import com.example.demo.model.UserPosition;
import com.example.demo.repository.UserPositionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserPositionService {
    @Autowired
    private UserPositionRepository userPositionRepository;

    public Iterable<UserPosition> getAllUserPosition() {
        return userPositionRepository.findAll();
    }

    public UserPosition userPositionById(int id) {
        Optional<UserPosition> userPosition = userPositionRepository.findById(id);
        if (userPosition.isPresent()) {
            return userPosition.get();
        }
        return null;
    }

    public UserPosition createUserPosition(UserPosition userPosition) {
        return userPositionRepository.save(userPosition);
    }

    public UserPosition changeUserPosition(UserPosition userPosition) {
        return userPositionRepository.save(userPosition);
    }

    public void deleteUserPosition(int id) {
        try {
            userPositionRepository.deleteById(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
