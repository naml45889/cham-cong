package com.example.demo.service;

import com.example.demo.DTO.UserdDTO;
import com.example.demo.model.User;
import com.example.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    public Iterable<UserdDTO> getAlluser(int page) {
        List<User> users = userRepository.findAll(PageRequest.of(page - 1, 5));
        List<UserdDTO> userdDTOS = new ArrayList<>();
        for (int i = 0; i < users.size(); i++) {
            UserdDTO userdDTO=new UserdDTO();
            userdDTO.setUser(users.get(i));
            userdDTOS.add(userdDTO);
        }
        return userdDTOS;
    }
    //test
    public List<User> userByDepartmentId(int departmentId){
        return userRepository.findByDepartmentId(departmentId);
    }
    //test
    public User userById(int id) {
        Optional<User> user = userRepository.findById(id);
        if (user.isPresent()) {
            return user.get();
        }
        return null;
    }

    public User userByUsername(String username) {
        Optional<User> user = userRepository.findByUsername(username);
        if (user.isPresent()) {
            return user.get();
        }
        return null;
    }

    public List<User> UserByDepartment(int departmentId) {
        return userRepository.findByDepartmentId(departmentId);
    }

    public User createUser(@Valid User user) {
        return userRepository.save(user);
    }

    public User changeUser(@Valid User user) {
        return userRepository.save(user);
    }

    public void deleteUser(int id) {
        try {
            userRepository.deleteById(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
