package com.example.demo.service;

import com.example.demo.model.Managerment;
import com.example.demo.repository.ManagermentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ManagermentService {
    @Autowired
    private ManagermentRepository managermentRepository;

    public Iterable<Managerment> getAllMangerment() {
        return managermentRepository.findAll();
    }

    public Managerment managermentById(int id) {
        Optional<Managerment> managerment = managermentRepository.findById(id);
        if (managerment.isPresent()) {
            return managerment.get();
        }
        return null;
    }

    public List<Managerment> managermentByUserId(int userId) {
        List<Managerment> managerment = managermentRepository.findByUserId(userId);
        return managerment;
    }

    public Managerment createManagerment(Managerment managerment) {
        return managermentRepository.save(managerment);
    }

    public Managerment changeManagerment(Managerment managerment) {
        return managermentRepository.save(managerment);
    }

    public void deleteManagerment(int id) {
        try {
            managermentRepository.deleteById(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
