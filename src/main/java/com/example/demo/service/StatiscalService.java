package com.example.demo.service;

import com.example.demo.DTO.StatisticalDTO;
import com.example.demo.model.Access;
import com.example.demo.model.Request;
import com.example.demo.model.User;
import com.example.demo.repository.AccessRepository;
import com.example.demo.repository.RequestRepository;
import com.example.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;


@Service
public class StatiscalService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private AccessRepository accessRepository;
    @Autowired
    private RequestRepository requestRepository;

    public StatisticalDTO findStatiscalByUser(int id) {
        StatisticalDTO statisticalDTO=new StatisticalDTO();
        Optional<User> user = userRepository.findById(id);
        statisticalDTO.setUser(user.get());
        List<Access> accesses = (List<Access>) accessRepository.findByUserId(id);
        // tinh tong so gio di lam
        float totalHour = 0;
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm");
        for (int i = 0; i < accesses.size() - 1; i++) {
            String date1 = String.valueOf(accesses.get(i).getDate());
            String date2 = String.valueOf(accesses.get(i + 1).getDate());
            if (date1.equals(date2)) {
                LocalTime time = accesses.get(i).getTime();
                String time1 = time.truncatedTo(ChronoUnit.SECONDS).format(dtf);
                float result = Float.parseFloat(time1.replace(":", "."));

                LocalTime timee = accesses.get(i + 1).getTime();
                String timee1 = timee.truncatedTo(ChronoUnit.SECONDS).format(dtf);
                float reesult = Float.parseFloat(timee1.replace(":", "."));
                totalHour += reesult - result;
                i++;
            }
        }
        statisticalDTO.setTotalHour(totalHour);
        // tinh tong so gio di muon
        float totalLate = 0;// tong so gio di muon
        LocalTime checkIn = user.get().getCheckIn();
        String checkInTime = checkIn.truncatedTo(ChronoUnit.SECONDS).format(dtf);
        float checkInExpected = Float.parseFloat(checkInTime.replace(":", "."));// thoi gian check in da dang ky
        for (int i = 0; i < accesses.size(); i++) {
            if(i==0){
                LocalTime checkInReality = accesses.get(0).getTime();
                String checkInTimeReality = checkInReality.truncatedTo(ChronoUnit.SECONDS).format(dtf);
                float checkInResultReality = Float.parseFloat(checkInTimeReality.replace(":", "."));// thoi gian check in thuc te
                if (checkInResultReality > checkInExpected) {
                    totalLate += checkInResultReality - checkInExpected;
                }
            }else{
                String date1 = String.valueOf(accesses.get(i).getDate());
                String date2 = String.valueOf(accesses.get(i - 1).getDate());
                // date1=date2 thi da check in => bo qua
                if(date1.equals(date2)){
                    i++;
                }else{
                    LocalTime checkInReality = accesses.get(i).getTime();
                    String checkInTimeReality = checkInReality.truncatedTo(ChronoUnit.SECONDS).format(dtf);
                    float checkInResultReality = Float.parseFloat(checkInTimeReality.replace(":", "."));
                    if (checkInResultReality > checkInExpected) {
                        totalLate += checkInResultReality - checkInExpected;
                    }
                }
            }
        }
        statisticalDTO.setLateHour(totalLate);
        // tong so gio OT
        statisticalDTO.setOt(user.get().getOT());
        // tinh tong so ngay nghi
        int totalDayOff=0;
        List<Request> requests=requestRepository.findByUserId(user.get().getId());
        for(int i=0;i<requests.size();i++){
            if(requests.get(i).getStatus()== Request.Status.APPROVED){
                totalDayOff++;
            }
        }
        statisticalDTO.setTotalDayOff(totalDayOff);
        return statisticalDTO;
    }
//    public StatisticalDTO findStatiscalByUser(int id, StatisticalDTO statisticalDTO) {
//        Optional<User> user = userRepository.findById(id);
//        statisticalDTO.setUser(user.get());
//        List<Access> accesses = (List<Access>) accessRepository.findByUserId(id);
//        // tinh tong so gio di lam
//        float totalHour = 0;
//        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm");
//        for (int i = 0; i < accesses.size() - 1; i++) {
//            String date1 = String.valueOf(accesses.get(i).getDate());
//            String date2 = String.valueOf(accesses.get(i + 1).getDate());
//            if (date1.equals(date2)) {
//                LocalTime time = accesses.get(i).getTime();
//                String time1 = time.truncatedTo(ChronoUnit.SECONDS).format(dtf);
//                float result = Float.parseFloat(time1.replace(":", "."));
//
//                LocalTime timee = accesses.get(i + 1).getTime();
//                String timee1 = timee.truncatedTo(ChronoUnit.SECONDS).format(dtf);
//                float reesult = Float.parseFloat(timee1.replace(":", "."));
//                totalHour += reesult - result;
//                i++;
//            }
//        }
//        statisticalDTO.setTotalHour(totalHour);
//        // tinh tong so gio di muon
//        float totalLate = 0;// tong so gio di muon
//        LocalTime checkIn = user.get().getCheckIn();
//        String checkInTime = checkIn.truncatedTo(ChronoUnit.SECONDS).format(dtf);
//        float checkInExpected = Float.parseFloat(checkInTime.replace(":", "."));// thoi gian check in da dang ky
//        for (int i = 0; i < accesses.size(); i++) {
//            if(i==0){
//                LocalTime checkInReality = accesses.get(0).getTime();
//                String checkInTimeReality = checkInReality.truncatedTo(ChronoUnit.SECONDS).format(dtf);
//                float checkInResultReality = Float.parseFloat(checkInTimeReality.replace(":", "."));// thoi gian check in thuc te
//                if (checkInResultReality > checkInExpected) {
//                    totalLate += checkInResultReality - checkInExpected;
//                }
//            }else{
//                String date1 = String.valueOf(accesses.get(i).getDate());
//                String date2 = String.valueOf(accesses.get(i - 1).getDate());
//                // date1=date2 thi da check in => bo qua
//                if(date1.equals(date2)){
//                    i++;
//                }else{
//                    LocalTime checkInReality = accesses.get(i).getTime();
//                    String checkInTimeReality = checkInReality.truncatedTo(ChronoUnit.SECONDS).format(dtf);
//                    float checkInResultReality = Float.parseFloat(checkInTimeReality.replace(":", "."));
//                    if (checkInResultReality > checkInExpected) {
//                        totalLate += checkInResultReality - checkInExpected;
//                    }
//                }
//            }
//        }
//        statisticalDTO.setLateHour(totalLate);
//        // tong so gio OT
//        statisticalDTO.setOt(user.get().getOT());
//        // tinh tong so ngay nghi
//        int totalDayOff=0;
//        List<Request> requests=requestRepository.findByUserId(user.get().getId());
//        for(int i=0;i<requests.size();i++){
//            if(requests.get(i).getStatus()== Request.Status.APPROVED){
//                totalDayOff++;
//            }
//        }
//        statisticalDTO.setTotalDayOff(totalDayOff);
//        return statisticalDTO;
//    }
}
