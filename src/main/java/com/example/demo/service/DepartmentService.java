package com.example.demo.service;

import com.example.demo.DTO.DepartmentDTO;
import com.example.demo.DTO.UserdDTO;
import com.example.demo.model.Department;
import com.example.demo.model.User;
import com.example.demo.repository.DepartmentRepository;
import com.example.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class DepartmentService {
    @Autowired
    private DepartmentRepository departmentRepository;
    @Autowired
    private UserRepository userRepository;

    public Iterable<Department> getAllDepartment() {
        return departmentRepository.findAll();
    }

    public DepartmentDTO departmentById(int id){
        Optional<Department> department = departmentRepository.findById(id);
        DepartmentDTO departmentDTO=new DepartmentDTO();
            departmentDTO.setName(department.get().getName());
            List<User> users = userRepository.findByDepartmentId(id);
            List<UserdDTO> userDepartment = new ArrayList<>();
            for (int i = 0; i < users.size(); i++) {
                UserdDTO userdDTO = new UserdDTO();
                userdDTO.setUser(users.get(i));
                userDepartment.add(userdDTO);
            }
            departmentDTO.setUsers(userDepartment);
            if (department.isPresent()) {
                return departmentDTO;
            }
        if (department.isPresent()) {
                return departmentDTO;
        }
        return null;
    }

    public Department createDepartment(Department Department) {
        return departmentRepository.save(Department);
    }

    public Department changeDepartment(Department Department) {
        return departmentRepository.save(Department);
    }

    public void deleteDepartment(int id) {
        try {
            departmentRepository.deleteById(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
