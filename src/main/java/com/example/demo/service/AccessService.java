package com.example.demo.service;

import com.example.demo.DTO.AccessDTO;
import com.example.demo.model.Access;
import com.example.demo.model.User;
import com.example.demo.repository.AccessRepository;
import com.example.demo.repository.UserRepository;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

@Service
public class AccessService {
    @Autowired
    private AccessRepository accessRepository;
    @Autowired
    private UserRepository userRepository;

    public Iterable<Access> getAllAccess() {
        return accessRepository.findAll();
    }

    public Access accessById(int id) {
        Optional<Access> access = accessRepository.findById(id);
        if (access.isPresent()) {
            return access.get();
        }
        return null;
    }

    public List<Access> accessByUser(int userId) {
        return accessRepository.findByUserId(userId);
    }

    public AccessDTO createAccess(String token) {
        byte[] decodedBytes = Base64.decode(token);
        if (decodedBytes == null) {
            return null;
        }
        String decodedString = new String(decodedBytes);
        String[] userDTo = decodedString.split(" ");
        String username = userDTo[0];
        String password = userDTo[1];
        Optional<User> user = userRepository.findByUsername(username);
        Access access=new Access();
        access.setUser(user.get());
        LocalDate date = LocalDate.now();
        access.setDate(date);
        LocalTime time = LocalTime.now();
        access.setTime(time);
        accessRepository.save(access);
        AccessDTO accessDTO=new AccessDTO();
        accessDTO.setAccess(access);
        return accessDTO;
    }

    public Access changeAccess(Access access) {
        return accessRepository.save(access);
    }

    public void deleteAccess(int id) {
        try {
            accessRepository.deleteById(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
