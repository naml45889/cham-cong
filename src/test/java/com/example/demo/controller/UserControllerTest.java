package com.example.demo.controller;

import com.example.demo.DTO.UserdDTO;
import com.example.demo.interceptor.AdminInterceptor;
import com.example.demo.interceptor.UserInterceptor;
import com.example.demo.model.User;
import com.example.demo.model.UserPosition;
import com.example.demo.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(UserController.class)
public class UserControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private UserService userService;
    @MockBean
    private UserInterceptor userInterceptor;
    @MockBean
    private AdminInterceptor adminInterceptor;
    @Before
    public void setup() throws Exception {
        // chan interceptor
        Mockito.when(userInterceptor.preHandle(any(),any(),any())).thenReturn(true);
        Mockito.when(adminInterceptor.preHandle(any(),any(),any())).thenReturn(true);
    }
    @Test
    public void testGetAllUser() throws Exception {
        int page=1;
        List<UserdDTO> userdDTOS = new ArrayList<>();
        UserdDTO userdDTO=new UserdDTO();
        userdDTO.setId(1);
        userdDTO.setName("test1");
        userdDTO.setSex(true);
        userdDTO.setEmail("namlc@dasd");
        userdDTO.setPosition(1);
        userdDTO.setBirthDay(LocalDate.now());
        userdDTOS.add(userdDTO);
        Mockito.when(userService.getAlluser(page)).thenReturn(userdDTOS);
        mockMvc.perform(MockMvcRequestBuilders
                .get("/user/get/")
                .param("page", String.valueOf(page))
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }
    public User user(){
        User user=new User();
        user.setId(1);
        user.setName("test1");
        user.setEmail("testemail");
        user.setPassword("testpass");
        user.setSex(true);
        user.setUsername("testUsername");
        user.setAdmin(true);
        user.setDayOff(1);
        user.setOT(1);
        UserPosition userPosition=new UserPosition();
        userPosition.setId(1);
        userPosition.setName("testPosition");
        userPosition.setLevel(1);
        user.setPosition(userPosition);
        return user;
    }
    @Test
    public void testUserById() throws Exception {
        User user=user();
        int id=1;
        Mockito.when(userService.userById(id)).thenReturn(user);
        mockMvc.perform(MockMvcRequestBuilders
                .get("/user/")
                .param("id", String.valueOf(id))
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }
    @Test
    public void createUser() throws Exception {
        User user=user();
        Mockito.when(userService.createUser(user)).thenReturn(user);

        mockMvc.perform(MockMvcRequestBuilders
                .post("/user/")
                .content(asJsonString(user))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(print());
    }
    @Test
    public void createUserErrorUsernameLesser() throws Exception {
        User user=user();
        user.setUsername("a");
        Mockito.when(userService.createUser(user)).thenReturn(user);

        mockMvc.perform(MockMvcRequestBuilders
                .post("/user/")
                .content(asJsonString(user))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andDo(print());
    }
    @Test
    public void createUserErrorUsernameGeater() throws Exception {
        User user=user();
        user.setUsername("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
        Mockito.when(userService.createUser(user)).thenReturn(user);

        mockMvc.perform(MockMvcRequestBuilders
                .post("/user/")
                .content(asJsonString(user))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andDo(print());
    }
    @Test
    public void createUserErrorPasswordLesser() throws Exception {
        User user=user();
        user.setPassword("a");
        Mockito.when(userService.createUser(user)).thenReturn(user);

        mockMvc.perform(MockMvcRequestBuilders
                .post("/user/")
                .content(asJsonString(user))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andDo(print());
    }
    @Test
    public void createUserErrorPasswordGreater() throws Exception {
        User user=user();
        user.setPassword("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
        Mockito.when(userService.createUser(user)).thenReturn(user);

        mockMvc.perform(MockMvcRequestBuilders
                .post("/user/")
                .content(asJsonString(user))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andDo(print());
    }
    @Test
    public void createUserErrorEmailLesser() throws Exception {
        User user=user();
        user.setEmail("a");
        Mockito.when(userService.createUser(user)).thenReturn(user);

        mockMvc.perform(MockMvcRequestBuilders
                .post("/user/")
                .content(asJsonString(user))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andDo(print());
    }
    @Test
    public void createUserErrorEmailGreater() throws Exception {
        User user=user();
        user.setEmail("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
        Mockito.when(userService.createUser(user)).thenReturn(user);

        mockMvc.perform(MockMvcRequestBuilders
                .post("/user/")
                .content(asJsonString(user))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andDo(print());
    }
    // edit user
    @Test
    public void editUser() throws Exception {
        User user=user();
        Mockito.when(userService.changeUser(user)).thenReturn(user);
        int id=1;
        mockMvc.perform(MockMvcRequestBuilders
                .put("/user/")
                .param("id", String.valueOf(id))
                .content(asJsonString(user))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(print());
    }
    @Test
    public void editUserErrorUsernameLesser() throws Exception {
        User user=user();
        user.setUsername("a");
        Mockito.when(userService.changeUser(user)).thenReturn(user);
        int id=1;
        mockMvc.perform(MockMvcRequestBuilders
                .put("/user/")
                .param("id", String.valueOf(id))
                .content(asJsonString(user))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andDo(print());
    }
    @Test
    public void editUserErrorUsernameGeater() throws Exception {
        User user=user();
        user.setUsername("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
        Mockito.when(userService.changeUser(user)).thenReturn(user);

        int id=1;
        mockMvc.perform(MockMvcRequestBuilders
                .put("/user/")
                .param("id", String.valueOf(id))
                .content(asJsonString(user))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andDo(print());
    }
    @Test
    public void editUserErrorPasswordLesser() throws Exception {
        User user=user();
        user.setPassword("a");
        Mockito.when(userService.changeUser(user)).thenReturn(user);

        int id=1;
        mockMvc.perform(MockMvcRequestBuilders
                .put("/user/")
                .param("id", String.valueOf(id))
                .content(asJsonString(user))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andDo(print());
    }
    @Test
    public void editUserErrorPasswordGreater() throws Exception {
        User user=user();
        user.setPassword("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
        Mockito.when(userService.changeUser(user)).thenReturn(user);

        int id=1;
        mockMvc.perform(MockMvcRequestBuilders
                .put("/user/")
                .param("id", String.valueOf(id))
                .content(asJsonString(user))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andDo(print());
    }
    @Test
    public void editUserErrorEmailLesser() throws Exception {
        User user=user();
        user.setEmail("a");
        Mockito.when(userService.changeUser(user)).thenReturn(user);

        int id=1;
        mockMvc.perform(MockMvcRequestBuilders
                .put("/user/")
                .param("id", String.valueOf(id))
                .content(asJsonString(user))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andDo(print());
    }
    @Test
    public void editUserErrorEmailGreater() throws Exception {
        User user=user();
        user.setEmail("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
        Mockito.when(userService.changeUser(user)).thenReturn(user);

        int id=1;
        mockMvc.perform(MockMvcRequestBuilders
                .put("/user/")
                .param("id", String.valueOf(id))
                .content(asJsonString(user))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andDo(print());
    }
    // delete user
    @Test
    public void deleteUser() throws Exception {
        User user=user();
        int id=1;
        Mockito.when(userService.userById(id)).thenReturn(user);
        doNothing().when(userService).deleteUser(id);
        mockMvc.perform(MockMvcRequestBuilders
                .delete("/user/")
                .param("id", String.valueOf(id))
                .content(asJsonString(user))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(print());
    }
    // delete user exception
    @Test(expected = Exception.class)
    public void deleteUserException() throws Exception {
        User user=user();
        int id=1;
        Mockito.when(userService.userById(id)).thenReturn(user);
        doThrow(Exception.class).when(userService).deleteUser(id);
        mockMvc.perform(MockMvcRequestBuilders
                .delete("/user/")
                .param("id", String.valueOf(id))
                .content(asJsonString(user))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(print());
    }
    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
