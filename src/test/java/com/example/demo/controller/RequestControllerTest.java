package com.example.demo.controller;

import com.example.demo.DTO.RequestDTO;
import com.example.demo.DTO.RequestdDTO;
import com.example.demo.interceptor.AdminInterceptor;
import com.example.demo.interceptor.UserInterceptor;
import com.example.demo.model.Request;
import com.example.demo.model.User;
import com.example.demo.model.UserPosition;
import com.example.demo.service.RequestService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.LocalDate;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(RequestController.class)
public class RequestControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private RequestService requestService;
    @MockBean
    private UserInterceptor userInterceptor;
    @MockBean
    private AdminInterceptor adminInterceptor;
    @Before
    public void setup() throws Exception {
        // chan interceptor
        Mockito.when(userInterceptor.preHandle(any(),any(),any())).thenReturn(true);
        Mockito.when(adminInterceptor.preHandle(any(),any(),any())).thenReturn(true);
    }
    public User user(){// khoi tao doi tuong user
        User user=new User();
        user.setId(1);
        user.setName("namlc");
        user.setEmail("testemail");
        user.setPassword("12345");
        user.setSex(true);
        user.setUsername("namlc");
        user.setAdmin(true);
        user.setDayOff(1);
        user.setOT(1);
        UserPosition userPosition=new UserPosition();
        userPosition.setId(1);
        userPosition.setName("testPosition");
        userPosition.setLevel(1);
        user.setPosition(userPosition);
        return user;
    }

    @Test
    public void editRequest() throws Exception {// chinh sua don xin nghi(token la cua manager, id cua request)
        int id=1;// id cua request
        RequestDTO requestDTO=new RequestDTO();// dau vao
        requestDTO.setStatus(Request.Status.APPROVED);

        RequestdDTO requestdDTO=new RequestdDTO();// ket qua mong doi
        requestdDTO.setReason("xin nghi om");
        requestdDTO.setStart(LocalDate.now());
        requestdDTO.setEnd(LocalDate.now());
        requestdDTO.setPosition(1);
        requestdDTO.setEmail("testEmail");
        requestdDTO.setName("testname");
        requestdDTO.setStatus(Request.Status.APPROVED);

        User manager=user();// quan ly co the duyet don xin nghi cua nhan vien
        manager.setId(2);
        String token1 = manager.getUsername() + " " + manager.getPassword();
        String token = Base64.encode(token1.getBytes());

        Mockito.when(requestService.changeRequest(id,token,requestDTO)).thenReturn(requestdDTO);
        mockMvc.perform(MockMvcRequestBuilders
                .put("/request/")
                .header("token",token)
                .param("id", String.valueOf(id))
                .content(asJsonString(requestDTO))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(print());
    }
    // request null
    @Test
    public void editRequestErrorNull() throws Exception {// chinh sua don xin nghi(token la cua manager, id cua request)
        int id=1;// id cua request
        RequestDTO requestDTO=new RequestDTO();// dau vao
        requestDTO.setStatus(Request.Status.APPROVED);

        RequestdDTO requestdDTO=null;// ket qua mong doi

        User manager=user();// quan ly co the duyet don xin nghi cua nhan vien
        manager.setId(2);
        String token1 = manager.getUsername() + " " + manager.getPassword();
        String token = Base64.encode(token1.getBytes());

        Mockito.when(requestService.changeRequest(id,token,requestDTO)).thenReturn(requestdDTO);
        mockMvc.perform(MockMvcRequestBuilders
                .put("/request/")
                .header("token",token)
                .param("id", String.valueOf(id))
                .content(asJsonString(requestDTO))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andDo(print());
    }
    @Test
    public void createRequest() throws Exception {// tao don xin nghi
        int id=1;// id cua request

        User user=user();// nhan vien viet don xin nghi
        String token1 = user.getUsername() + " " + user.getPassword();
        String token = Base64.encode(token1.getBytes());

        RequestdDTO requestdDTO=new RequestdDTO();// ket qua mong doi
        requestdDTO.setStatus(Request.Status.NEW);
        requestdDTO.setEmail(user.getEmail());
        requestdDTO.setReason("xin nghi om");
        requestdDTO.setPosition(user.getPosition().getId());
        requestdDTO.setStart(LocalDate.parse("2021-01-23"));
        requestdDTO.setEnd(LocalDate.parse("2021-01-23"));

        Request request=new Request();// dau vao
        request.setUser(user);
        request.setStatus(Request.Status.NEW);
        request.setId(1);
        request.setReason("xin nghi om");

        Mockito.when(requestService.createRequest(user.getId(),token,request)).thenReturn(requestdDTO);
        mockMvc.perform(MockMvcRequestBuilders
                .post("/request/")
                .header("token",token)
                .param("id", String.valueOf(id))
                .content(asJsonString(request))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(print());
    }
    // reason<5
    @Test
    public void createRequestErrorReasonLesser() throws Exception {// tao don xin nghi
        int id=1;// id cua request

        User user=user();// nhan vien viet don xin nghi
        String token1 = user.getUsername() + " " + user.getPassword();
        String token = Base64.encode(token1.getBytes());

        RequestdDTO requestdDTO=new RequestdDTO();// ket qua mong doi
        requestdDTO.setStatus(Request.Status.NEW);
        requestdDTO.setEmail(user.getEmail());
        requestdDTO.setReason("x");
        requestdDTO.setPosition(user.getPosition().getId());
        requestdDTO.setStart(LocalDate.parse("2021-01-23"));
        requestdDTO.setEnd(LocalDate.parse("2021-01-23"));

        Request request=new Request();// dau vao
        request.setUser(user);
        request.setStatus(Request.Status.NEW);
        request.setId(1);
        request.setReason("x");

        Mockito.when(requestService.createRequest(user.getId(),token,request)).thenReturn(requestdDTO);
        mockMvc.perform(MockMvcRequestBuilders
                .post("/request/")
                .header("token",token)
                .param("id", String.valueOf(id))
                .content(asJsonString(request))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andDo(print());
    }
    // reason>30
    @Test
    public void createRequestErrorReasonGreater() throws Exception {// tao don xin nghi
        int id=1;// id cua request

        User user=user();// nhan vien viet don xin nghi
        String token1 = user.getUsername() + " " + user.getPassword();
        String token = Base64.encode(token1.getBytes());

        RequestdDTO requestdDTO=new RequestdDTO();// ket qua mong doi
        requestdDTO.setStatus(Request.Status.NEW);
        requestdDTO.setEmail(user.getEmail());
        requestdDTO.setReason("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
        requestdDTO.setPosition(user.getPosition().getId());
        requestdDTO.setStart(LocalDate.parse("2021-01-23"));
        requestdDTO.setEnd(LocalDate.parse("2021-01-23"));

        Request request=new Request();// dau vao
        request.setUser(user);
        request.setStatus(Request.Status.NEW);
        request.setId(1);
        request.setReason("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");

        Mockito.when(requestService.createRequest(user.getId(),token,request)).thenReturn(requestdDTO);
        mockMvc.perform(MockMvcRequestBuilders
                .post("/request/")
                .header("token",token)
                .param("id", String.valueOf(id))
                .content(asJsonString(request))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andDo(print());
    }
    // request null
    @Test
    public void createRequestErrorNull() throws Exception {// tao don xin nghi
        int id=1;// id cua request

        User user=user();// nhan vien viet don xin nghi
        String token1 = user.getUsername() + " " + user.getPassword();
        String token = Base64.encode(token1.getBytes());

        RequestdDTO requestdDTO=null;// ket qua mong doi

        Request request=new Request();// dau vao
        request.setUser(user);
        request.setStatus(Request.Status.NEW);
        request.setId(1);
        request.setReason("xin nghi om");

        Mockito.when(requestService.createRequest(user.getId(),token,request)).thenReturn(requestdDTO);
        mockMvc.perform(MockMvcRequestBuilders
                .post("/request/")
                .header("token",token)
                .param("id", String.valueOf(id))
                .content(asJsonString(request))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andDo(print());
    }
    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
