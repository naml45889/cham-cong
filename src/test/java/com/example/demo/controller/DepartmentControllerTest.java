package com.example.demo.controller;

import com.example.demo.DTO.DepartmentDTO;
import com.example.demo.DTO.UserdDTO;
import com.example.demo.interceptor.AdminInterceptor;
import com.example.demo.interceptor.UserInterceptor;
import com.example.demo.model.User;
import com.example.demo.model.UserPosition;
import com.example.demo.service.DepartmentService;
import com.example.demo.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
@RunWith(SpringRunner.class)
@WebMvcTest(DepartmentController.class)
public class DepartmentControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private UserService userService;
    @MockBean
    private DepartmentService departmentService;
    @MockBean
    private UserInterceptor userInterceptor;
    @MockBean
    private AdminInterceptor adminInterceptor;
    @Before
    public void setup() throws Exception {
        // chan interceptor
        Mockito.when(userInterceptor.preHandle(any(),any(),any())).thenReturn(true);
        Mockito.when(adminInterceptor.preHandle(any(),any(),any())).thenReturn(true);
    }
    @Test
    public void testGetDepartment() throws Exception {
        List<UserdDTO> userdDTOS = new ArrayList<>();
        UserdDTO userdDTO=new UserdDTO();
        userdDTO.setUser(user());
        userdDTO.setId(1);
        userdDTO.setName("test1");
        userdDTO.setSex(true);
        userdDTO.setEmail("namlc@dasd");
        userdDTO.setPosition(1);
        userdDTO.setBirthDay(LocalDate.now());
        userdDTOS.add(userdDTO);

        DepartmentDTO departmentDTO=new DepartmentDTO();
        departmentDTO.setName("test");
        departmentDTO.setUsers(userdDTOS);
        int id=1;
        Mockito.when(departmentService.departmentById(id)).thenReturn(departmentDTO);
        mockMvc.perform(MockMvcRequestBuilders
                .get("/department/")
                .param("id", String.valueOf(id))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }
    public User user(){
        User user=new User();
        user.setId(1);
        user.setName("test1");
        user.setEmail("testemail");
        user.setPassword("testpass");
        user.setSex(true);
        user.setUsername("testUsername");
        user.setAdmin(true);
        user.setDayOff(1);
        user.setOT(1);
        UserPosition userPosition=new UserPosition();
        userPosition.setId(1);
        userPosition.setName("testPosition");
        userPosition.setLevel(1);
        user.setPosition(userPosition);
        return user;
    }

}
