package com.example.demo.controller;

import com.example.demo.DTO.StatisticalDTO;
import com.example.demo.interceptor.AdminInterceptor;
import com.example.demo.interceptor.UserInterceptor;
import com.example.demo.model.User;
import com.example.demo.model.UserPosition;
import com.example.demo.service.StatiscalService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(StatisticalController.class)
public class StatiscalControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private StatiscalService statiscalService;
    @MockBean
    private UserInterceptor userInterceptor;
    @MockBean
    private AdminInterceptor adminInterceptor;
    @Before
    public void setup() throws Exception {
        // chan interceptor
        Mockito.when(userInterceptor.preHandle(any(),any(),any())).thenReturn(true);
        Mockito.when(adminInterceptor.preHandle(any(),any(),any())).thenReturn(true);
    }
    public User user(){
        User user=new User();
        user.setId(1);
        user.setName("test1");
        user.setEmail("testemail");
        user.setPassword("testpass");
        user.setSex(true);
        user.setUsername("testUsername");
        user.setAdmin(true);
        user.setDayOff(1);
        user.setOT(1);
        UserPosition userPosition=new UserPosition();
        userPosition.setId(1);
        userPosition.setName("testPosition");
        userPosition.setLevel(1);
        user.setPosition(userPosition);
        return user;
    }
    @Test
    public void testGetStatiscal() throws Exception {
        User user=user();
        StatisticalDTO statisticalDTO=new StatisticalDTO();
        statisticalDTO.setUser(user);
        statisticalDTO.setLateHour(1);
        statisticalDTO.setTotalHour(2);
        statisticalDTO.setTotalDayOff(3);
        Mockito.when(statiscalService.findStatiscalByUser(user.getId())).thenReturn(statisticalDTO);
        mockMvc.perform(MockMvcRequestBuilders
                .get("/statistical/")
                .param("id", String.valueOf(user.getId()))
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }
}
