package com.example.demo.controller;

import com.example.demo.DTO.AccessDTO;
import com.example.demo.interceptor.AdminInterceptor;
import com.example.demo.interceptor.UserInterceptor;
import com.example.demo.model.User;
import com.example.demo.model.UserPosition;
import com.example.demo.service.AccessService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.LocalDate;
import java.time.LocalTime;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(AccessController.class)
public class AccessControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private AccessService accessService;
    @MockBean
    private UserInterceptor userInterceptor;
    @MockBean
    private AdminInterceptor adminInterceptor;
    @Before
    public void setup() throws Exception {
        // chan interceptor
        Mockito.when(userInterceptor.preHandle(any(),any(),any())).thenReturn(true);
        Mockito.when(adminInterceptor.preHandle(any(),any(),any())).thenReturn(true);
    }
    public User user(){
        User user=new User();
        user.setId(1);
        user.setName("test1");
        user.setEmail("testemail");
        user.setPassword("testpass");
        user.setSex(true);
        user.setUsername("testUsername");
        user.setAdmin(true);
        user.setDayOff(1);
        user.setOT(1);
        UserPosition userPosition=new UserPosition();
        userPosition.setId(1);
        userPosition.setName("testPosition");
        userPosition.setLevel(1);
        user.setPosition(userPosition);
        return user;
    }

    @Test
    public void createAccess() throws Exception { // check in - check out
        User user=user();

        String token1 = user.getUsername() + " " + user.getPassword();
        String token = Base64.encode(token1.getBytes());

        AccessDTO accessDTO=new AccessDTO();// ket qua tra ve
        accessDTO.setEmail(user.getEmail());
        accessDTO.setName(user.getName());
        accessDTO.setPosition(user.getPosition().getId());
        accessDTO.setUsername(user.getUsername());
        accessDTO.setDate(LocalDate.now());
        accessDTO.setTime(LocalTime.now());

        Mockito.when(accessService.createAccess(token)).thenReturn(accessDTO);
        mockMvc.perform(MockMvcRequestBuilders
                .post("/check/")
                .header("token",token)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(print());
    }
    // error access null
    @Test
    public void createAccessErrorNull() throws Exception {
        User user=user();

        String token1 = user.getUsername() + " " + user.getPassword();
        String token = Base64.encode(token1.getBytes());

        AccessDTO accessDTO=null;// ket qua tra ve

        Mockito.when(accessService.createAccess(token)).thenReturn(accessDTO);
        mockMvc.perform(MockMvcRequestBuilders
                .post("/check/")
                .header("token",token)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andDo(print());
    }
    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
